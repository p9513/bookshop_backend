package com.example.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.entity.Auxtransction;

public interface AuxtransctionDao extends JpaRepository<Auxtransction, Integer> {

}
