package com.example.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.entity.Books;

public interface BooksDao extends JpaRepository<Books, Integer> {

}
