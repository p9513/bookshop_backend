package com.example.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.entity.Transction;

public interface TransctionDao extends JpaRepository<Transction, Integer> {

}
