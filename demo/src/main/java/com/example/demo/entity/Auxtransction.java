package com.example.demo.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor  
@AllArgsConstructor  
@ToString 
@Entity
@Table(name="auxtransction")
public class Auxtransction {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private @Setter @Getter int auxId;
	
	@ManyToOne
	@JoinColumn(name="trdId")
	 private @Getter @Setter Transction transction;
	
	@ManyToOne
	@JoinColumn(name="bookId")
	 private @Getter @Setter Books books;
}
