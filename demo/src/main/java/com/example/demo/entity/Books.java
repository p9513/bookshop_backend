package com.example.demo.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
@NoArgsConstructor  
@AllArgsConstructor  
@ToString 

@Entity
@Table(name="books")
public class Books {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
 private int bookId;
 private @Getter @Setter String name;
 private @Getter @Setter String author;
 private @Getter @Setter String image;
 private @Getter @Setter  String category;
 private @Getter @Setter String decription;
 private @Getter @Setter double price;
 private @Getter @Setter int quantity;
 @OneToOne @JoinColumn(name="owner") 
 private @Getter @Setter User owner;
}
