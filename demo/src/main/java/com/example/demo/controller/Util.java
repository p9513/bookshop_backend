package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dao.UserDao;
import com.example.demo.entity.User;
import com.example.demo.model.Credentials;
import com.example.demo.services.UserService;

@RestController
@CrossOrigin
public class Util {
	
	@Autowired
	UserService userService;
	
	@PostMapping("/authenticate")
	private ResponseEntity<?> authenticate(Credentials cred) {
		User user = null;
		if(cred.getEmail()!=null && cred.getPassword()!=null)
			user=userService.authenticate(cred);
		if(user!=null)
			return ResponseEntity.ok(user);
		return ResponseEntity.badRequest().body("Invalid Credentials");
	}
}
