package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
//@ComponentScan("com.example.Controller")
//@ComponentScan()
//@ComponentScan()
//@ComponentScan()
//@ComponentScan()
//@ComponentScan()
//@ComponentScan({"com.example.demo","com.example.entity","com.example.model","com.example.dao","com.example.services","com.example.Controller"})
@SpringBootApplication
//@SpringBootConfiguration
@EnableAutoConfiguration(exclude = SecurityAutoConfiguration.class)
public class DemoApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

}
