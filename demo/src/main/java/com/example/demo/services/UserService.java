package com.example.demo.services;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.UserDao;
import com.example.demo.entity.User;
import com.example.demo.model.Credentials;

@Transactional
@Service
public class UserService {
	
	@Autowired(required=true)
	public UserDao userDao;
	
	public User authenticate(Credentials cred) {
		User user = userDao.findByEmailId(cred.getEmail());
		if(user != null && cred.getPassword().equals(user.getPassword()))
			return user;
		 return null;
	}
}
